<?php
global $post;




if (isset($GLOBALS['pix_footer_type_page']) && $GLOBALS['pix_footer_type_page'] && $GLOBALS['pix_footer_type_page'] != 'global'){
	$footerBlockId = $GLOBALS['pix_footer_type_page'];
}else{
	$footerBlockId = pixtheme_get_option('pix_footer_staticblock');

}



if ($footerBlockId){
	$post = $fpost = get_post( $footerBlockId );
	$fslug = esc_attr($fpost->post_name);


	$pix_decor = get_post_meta($footerBlockId, 'pix_page_landing_decor', true);

	$pix_preset = get_post_meta($footerBlockId, 'homepage_preset', true);
	$pix_preset_text = get_post_meta($footerBlockId, 'homepage_preset_text', true);
	$pix_slider = rwmb_meta('sequence_upload', 'type=image&size=full',$footerBlockId);




	$pixtheme_slider = get_post_meta($footerBlockId, 'homepage_slider', true);
	$out_slider = "";

	foreach($pix_slider as $slide) {
		$out_slider .= '<li><div style="background-image:url(' . esc_url($slide['url']) . ')" class="bg-slide"></div></li>';
	}
	$custom_color = get_post_meta($footerBlockId, 'cs_homepage_bgcolor', true);
	$bg_image = get_post_meta($footerBlockId, 'homepage_bgimage', true);
	$src = wp_get_attachment_image_src($bg_image, 'full');

	$style = ($bg_image) ?'background-image:url('.esc_url($src[0]).');':'';
	$bg_color = ($custom_color) ? 'background-color:'.esc_attr($custom_color) : '';
	$class_preset = ($pix_preset) ? 'no-bg-color-parallax parallax-'.$pix_preset.' ' : '';
	$class_preset_text = ($pix_preset_text) ? ' '.$pix_preset_text : '';

	$_no_padding = get_post_meta($footerBlockId, 'pix_page_section_nopadding', true);

	$class_preset_padding = ($_no_padding) ? ' no-padding' : '';
	$class = $class_preset.'home-section'.$class_preset_text.$class_preset_padding;
	$page_template_name = get_post_meta($footerBlockId,'_wp_page_template',true);

	$pix_decor_top = $out_slider != "" ? '' : '		<div class="section-footer ">
	<div class="sf-left" style="'.esc_attr($bg_color).'"></div>
	<div class="sf-right" style="'.esc_attr($bg_color).'"></div>

	</div>';

	$pix_decor_bottom = $out_slider != "" ? '' : '	<div class="section-footer sf-type-2">

	<div class="sf-left" style="'.esc_attr($bg_color).'"></div>
	<div class="sf-right" style="'.esc_attr($bg_color).'"></div>

	</div>';



}


?>
<?php	$contact = new WP_Query(array('post_type' => 'staticblocks','name'=>'contato-rodape'));?>
<?php while ($contact->have_posts()) : $contact->the_post(); ?>
	<div class="clearfix"></div>
	<?php if("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" != "https://trippaventura.com.br/formulario-contato/") the_content(); ?>
<?php	endwhile;	wp_reset_query(); ?>
<?php if ($footerBlockId):?>
	<footer class="footer footer-shop <?php echo esc_attr($fslug) ?> <?php echo esc_attr($class); ?>" style="<?php echo esc_attr($bg_color); ?>">
		<div class="container">
			<div class="col-sm-4">
				<?php	$footer_1 = new WP_Query(array('post_type' => 'staticblocks','name'=>'institucional'));?>
				<?php while ($footer_1->have_posts()) : $footer_1->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<div class="clearfix"></div>
					<?php the_content(); ?>
				<?php	endwhile;	wp_reset_query(); ?>
				
			</div>
			<div class="col-sm-4">
				
				<?php	$footer_1 = new WP_Query(array('post_type' => 'staticblocks','name'=>'formas-de-pagamento'));?>
				<?php while ($footer_1->have_posts()) : $footer_1->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<div class="clearfix"></div>
					<?php the_content(); ?>
				<?php	endwhile;	wp_reset_query(); ?>
			</div>
			<div class="col-sm-4">
				<!-- Godaddy -->
				<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=Jnh19SPXJ9ZAkxqf3Rd45huQljvoepRVBxBx54Zi8ZZXowY7gQX8KgXI2Thn"></script></span>
				<!-- Godaddy -->
			</div>


<!--
			<div class="col-sm-4">
				<?php	$footer_2 = new WP_Query(array('post_type' => 'staticblocks','name'=>'nossas-lojas'));?>
				<?php while ($footer_2->have_posts()) : $footer_2->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<div class="clearfix"></div>
					<?php the_content(); ?>
				<?php	endwhile;	wp_reset_query(); ?>
			</div>
			<div class="col-sm-4">
				<?php	$footer_2 = new WP_Query(array('post_type' => 'staticblocks','name'=>'facebook'));?>
				<?php while ($footer_2->have_posts()) : $footer_2->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<div class="clearfix"></div>
					<?php the_content(); ?>
				<?php	endwhile;	wp_reset_query(); ?>
			</div>
-->
		</div>
		<?php


		$shortcodes_custom_css = get_post_meta( $fpost->ID, '_wpb_shortcodes_custom_css', true );
		if ( ! empty( $shortcodes_custom_css ) ) {
			echo '<style type="text/css" data-type="vc_shortcodes-custom-css">';
			echo esc_html($shortcodes_custom_css);
			echo '</style>';
		}

	
		?>
	</footer>
        <!-- 
	<div class="assinatura">
		<img src="http://www.igorgottschalg.com/assinatura/logo-igorgottschalg.png" width="100"/> &
		<img src="http://www.beestart.com.br/wp-content/uploads/2015/10/beeLogo-30.png" width="80"/>
	</div>
        -->
<?php else: ?>




<?php endif; ?>

<?php $pix_options = isset($_POST['options']) ? $_POST['options'] : get_option('pix_general_settings');?>
<?php if (isset($pix_options['pix_custom_js'])) echo esc_js($pix_options['pix_custom_js']); ?>
</div>
<!--#page-content-wrapper-->
</div></div>
<!--#content-->
<!-- end layout-theme -->
</div>


<div class="la-anim-1"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=396103507255633";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php
wp_footer();
?>
<!--Start of Zopim Live Chat Script-->

<!--End of Zopim Live Chat Script-->

<!-- Código do Google para tag de remarketing -->
<!--------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 880590808;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/880590808/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Hotjar Tracking Code for http://www.trippaventura.com.br -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:226888,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</body></html>
