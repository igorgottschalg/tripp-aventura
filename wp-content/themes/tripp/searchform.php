
<form action="<?php echo esc_url(site_url()) ?>" method="get" id="search-global-form">
	<input type="text" placeholder="<?php _e('Search', 'PixTheme');?>" name="s" id="search" value="<?php esc_attr(the_search_query()); ?>" />
</form>

<script>
jQuery(function($){
	$("#search-global-form").submit(function(){
		if($("#search_filter_header_input").val() === ""){
			return false;
		}
	});
});
</script>
