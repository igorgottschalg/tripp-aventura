<?php get_header();?>


<main class="section" id="main">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-7 col-md-9  <?php if ($layout == '3'):?>  col2-left  <?php endif?>   <?php if ($layout == '2'):?>  col2-right  <?php endif?>">
        <section role="main" class="main-content page-404">
          <?php	$_404 = new WP_Query(array('post_type' => 'staticblocks','name'=>'nao-encontrado'));?>
          <?php while ($_404->have_posts()) : $_404->the_post(); ?>
          	<div class="clearfix"></div>
          	<?php the_content(); ?>
          <?php	endwhile;	wp_reset_query(); ?>
        </section>
      </div>
    </div>
  </div>
</main>
<?php get_footer(); ?>
