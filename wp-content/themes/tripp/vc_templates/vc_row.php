<?php
/** @var $this WPBakeryShortCode_VC_Row */
$output = $pdecor = $ptextcolor = $el_class = $bg_image = $bg_color = $bg_image_repeat = $font_color = $padding = $margin_bottom = $css = $full_width = '';
extract( shortcode_atts( array(
	'el_class' => '',
	'bg_image' => '',
	'bg_color' => '',
	'bg_image_repeat' => '',
	'font_color' => '',
	'padding' => '',
	'margin_bottom' => '',
	'full_width' => false,
	'css' => '',
	'pbgslides' => '',
	'pdecor' => '',
	'ptextcolor' => ''
), $atts ) );





$qo = get_queried_object();



if ($qo && isset($qo->ID)){
	
	$tpl_page =  get_page_template_slug($qo->ID);
}else{
	$tpl_page =  get_page_template();
}

if ( get_post_type( get_the_ID() ) == 'staticblocks'){
	$_block = get_post(get_the_ID());
	if ($_block && $_block->post_name == 'footer'){
		$full_width = array();
		$tpl_page = 'template-home.php';
	}
}


if ($pdecor == "Top")
	$pix_decor_class = " top-decor";
elseif ($pdecor == "Bottom")
	$pix_decor_class = " bottom-decor";	
elseif ($pdecor == "Both")
	$pix_decor_class = " both-decor";
else
	$pix_decor_class = "";
	
$pix_slider = rwmb_meta('sequence_upload', 'type=image&size=full');


$pix_slides = explode(",",$pbgslides);
$out_slider = "";
foreach($pix_slides as $slide) {
	$att_arr = wp_get_attachment_image_src($slide,'full');
	if (isset($att_arr[0])){
		$att = $att_arr[0];
		$out_slider .= '<li><div style="background-image:url(' . esc_url($att) . ')" class="bg-slide"></div></li>';					
	}

}	


$pix_decor_top =  $out_slider != "" ? '' :'		<div class="section-footer ">
													<div class="sf-left" style="'.esc_attr($bg_color).'"></div>
													<div class="sf-right" style="'.esc_attr($bg_color).'"></div>
												
												</div>';

$pix_decor_bottom = $out_slider != "" ? '' :'	<div class="section-footer sf-type-2">
												
													<div class="sf-left" style="'.esc_attr($bg_color).'"></div>
													<div class="sf-right" style="'.esc_attr($bg_color).'"></div>
											
												</div>';	


$class_preset_text = ($ptextcolor) ? ' text-'.strtolower($ptextcolor) : '';
if ($ptextcolor == "Default")
	$class_preset_text = "";


// wp_enqueue_style( 'js_composer_front' );
// wp_enqueue_style('js_composer_custom_css');

$el_class = $this->getExtraClass( $el_class );

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row ' . ( $this->settings( 'base' ) === 'vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$style = $this->buildStyle( $bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom );
?>
	<div <?php
	?>class="<?php echo esc_attr( $css_class . $pix_decor_class . $class_preset_text ); ?><?php if ( $full_width == 'stretch_row_content_no_spaces' ): echo ' vc_row-no-padding'; endif; ?>" <?php if ( ! empty( $full_width ) ) {
	echo ' data-vc-full-width="true"';
	if ( $full_width == 'stretch_row_content' || $full_width == 'stretch_row_content_no_spaces' ) {
		echo ' data-vc-stretch-content="true"';
	}
} ?> <?php echo $style; ?>>
<ul class="bg-slideshow">
    <?php echo $out_slider; ?>
  </ul>
<?php if(in_array($pdecor, array("Top","Both"))) echo wp_kses_post($pix_decor_top);?>
<?php if (empty( $full_width ) && $tpl_page == 'template-home.php' ):?>
<div class="container">
<?php endif;?>
<?php
echo wpb_js_remove_wpautop( $content );
?>
<?php if (empty( $full_width ) && $tpl_page == 'template-home.php' ):?>
</div>
<?php endif;?>
<?php if(in_array($pdecor, array("Bottom","Both"))) echo wp_kses_post($pix_decor_bottom);?>
</div><?php echo $this->endBlockComment( 'row' );
if ( ! empty( $full_width ) ) {
	echo '<div class="vc_row-full-width"></div>';
}
