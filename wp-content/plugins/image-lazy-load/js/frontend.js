/**
* Calls the jQuery Unveil library
*/
jQuery( document ).ready( function( $ ) {

	$( 'img[data-unveil="true"]' ).unveil( Number( image_lazy_load.image_unveil_load ), function() {
		$( this ).load( function() {
			// Fade in the image (combined with a CSS transition to complete the fade)
			this.style.opacity = 1;
		} );
	}) ;

});