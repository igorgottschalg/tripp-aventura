<?php
/**
* Plugin Name: Image Lazy Load (Unveil.js)
* Plugin URI: http://www.wpcube.co.uk/plugins/image-lazy-load
* Version: 1.0.9
* Author: WP Cube
* Author URI: http://www.wpcube.co.uk
* Description: Lazy load content images using the unveil.js jQuery plugin
* License: GPL2
*/

/*  Copyright 2015 WP Cube (email : support@wpcube.co.uk)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
* Image Lazy Load Class
* 
* @package WP Cube
* @subpackage Image Lazy Load
* @author Tim Carr
* @version 1.0.9
* @copyright WP Cube
*/
class Image_Lazy_Load {

    /**
    * Contains the default settings for a new plugin installation
    */
    public $settings = array(
        'load'   => 0,
        'mobile' => 0,
    );

    /**
    * Contains the success message when saving the plugin settings
    *
    * @since 1.0.8
    */
    public $message = '';

    /**
    * Contains any errors which occured when saving the plugin settings
    *
    * @since 1.0.8
    */
    public $errorMessage = '';

    /**
    * Constructor.
    */
    function __construct() {

        // Plugin Details
        $this->plugin               = new stdClass;
        $this->plugin->name         = 'image-lazy-load'; // Plugin Folder
        $this->plugin->displayName  = 'Image Lazy Load'; // Plugin Name
        $this->plugin->version      = '1.0.9';
        $this->plugin->folder       = plugin_dir_path( __FILE__ );
        $this->plugin->url          = plugin_dir_url( __FILE__ );

        // Upgrade Reasons
        $this->plugin->upgradeReasons = array();
        $this->plugin->upgradeReasons[] = array(
            __( 'Horizontal Scrolling', $this->plugin->name ), 
            __( 'Supports themes which use horizontal scrolling by unveiling images as they appear into view', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Responsive Support', $this->plugin->name ), 
            __( 'Full support for lazy loading responsive images which use srcset and size attributes (WordPress 4.4+)', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Featured Image Support', $this->plugin->name ), 
            __( 'Lazy loads Featured Images automatically', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Lazy Load Iframes', $this->plugin->name ), 
            __( 'Optionally choose to lazy load iframes, including YouTube videos', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Lazy Load Videos', $this->plugin->name ), 
            __( 'Optionally choose to lazy load &lt;video&gt; elements', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Animations', $this->plugin->name ), 
            __( 'Choose from 17 animations to use when images are displayed (including fade in, bounce, slide in etc)', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Loading Animation', $this->plugin->name ), 
            __( 'Upload any loading animation image, such as a spinner, which displays until the image is loaded', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Loading Animation Background', $this->plugin->name ), 
            __( 'Optionally define a background color for your loading animation whilst images are loaded into view', $this->plugin->name ),
        );
        $this->plugin->upgradeReasons[] = array(
            __( 'Disable', $this->plugin->name ), 
            __( 'Disable lazy loading by individual Post, or on specific Post Types and/or Taxonomies', $this->plugin->name ),
        );
        $this->plugin->upgradeURL = 'http://www.wpcube.co.uk/plugins/image-lazy-load-pro';

        // Dashboard Submodule
        if ( ! class_exists( 'WPCubeDashboardWidget' ) ) {
			require_once( $this->plugin->folder . '/_modules/dashboard/dashboard.php' );
		}
		$dashboard = new WPCubeDashboardWidget($this->plugin); 
		
		// Hooks
        add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
        add_action( 'plugins_loaded', array( &$this, 'load_language_files'));
        add_action( 'wp_enqueue_scripts', array( &$this, 'frontend_scripts_css' ) );
        
        // Activation
        register_activation_hook( __FILE__, array( &$this, 'activate' ) );
    }

    /**
     * Activation routine. Run when the plugin is activated
     *
     * @since 1.0.0
     */
    function activate() {
        
        // Attempt to get the settings for the plugin
        $settings_exist = get_option( $this->plugin->name );
        if ( ! $settings_exist || empty( $settings_exists ) ) {
            // No settings exist, so save the defaults
            update_option( $this->plugin->name, $this->settings );
        }

    }
    
    /**
    * Register the plugin settings panel
    *
    * @since 1.0.0
    */
    function admin_menu() {

        add_menu_page( $this->plugin->displayName, $this->plugin->displayName, 'manage_options', $this->plugin->name, array( &$this, 'admin_screen' ), 'dashicons-images-alt' );
    
    }
    
	/**
    * Output the Administration Panel
    * Save POSTed data from the Administration Panel into a WordPress option
    *
    * @since 1.0.0
    */
    function admin_screen() {

        // Save Settings
        if (isset($_POST['submit'])) {

            // Run security checks
            // Missing nonce 
            if ( ! isset( $_POST[ $this->plugin->name . '_nonce' ] ) ) { 
                $this->errorMessage = __( 'Nonce field is missing. Settings NOT saved.', $this->plugin->name );
            } elseif ( ! wp_verify_nonce( $_POST[$this->plugin->name.'_nonce'], $this->plugin->name ) ) {
                $this->errorMessage = __('Invalid nonce specified. Settings NOT saved.', $this->plugin->name );
            } else {
                // Get settings from POST data
                $settings = $_POST[ $this->plugin->name ];
                
                // Format and sanitize
                $settings['load'] = absint( $settings['load'] );
                $settings['mobile'] = ( isset( $settings['mobile'] ) ? 1 : 0 );

                // Save settings
                update_option( $this->plugin->name, $settings );

                // Set success message
                $this->message = __( 'Settings Updated.', $this->plugin->name );
            }
            
        }
        
        // Get latest settings
        $this->settings = get_option( $this->plugin->name );
        
		// Load Settings Form
        include_once( $this->plugin->folder . '/views/settings.php' );  

    }
    
    /**
     * Loads the plugin textdomain for translation.
     *
     * Uses wordpress.org's translations instead of supplied PO/MO files
     *
     * @since 1.0.8
     */
    function load_language_files() {

        load_plugin_textdomain( 'image-lazy-load' );

    }

    /**
    * Register and enqueue and JS and CSS for the WordPress Frontend, if image lazy loading is enabled
    *
    * @since 1.0.0
    */
    function frontend_scripts_css() {

        // Get settings
        $this->settings = get_option( $this->plugin->name );

        // Check if we're on a mobile device
        $is_mobile = $this->is_mobile();

        // If we're on mobile and loading on mobile is disabled, bail
        if ( $is_mobile && ! $this->settings['mobile'] ) {
            return;
        }

        // CSS
        wp_enqueue_style( $this->plugin->name . '-frontend', $this->plugin->url . 'css/frontend.css', array(), $this->plugin->version); 

        // JS
        wp_enqueue_script( $this->plugin->name . '-unveil' , $this->plugin->url . 'js/min/frontend-min.js', array( 'jquery' ), $this->plugin->version, true );
        wp_localize_script( $this->plugin->name . '-unveil', 'image_lazy_load', array(
            'image_unveil_load' => $this->settings['load'],
        ) );

        // Add a filter to the_content, so we can replace images as we know lazy loading is enabled
        add_filter( 'the_content', array( &$this, 'replace_images' ) );

    }

    /**
    * Checks if the device viewing a page is a mobile
    *
    * @return bool Is Mobile
    */
    function is_mobile() {

        // Get the user agent
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        // Check if the user is on a mobile device
        if( preg_match( '/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
            // Is a mobile device
            return true;
        }

        // Not a mobile device
        return false; 

    }

    /**
    * Replaces all <img> tags in the content with appropriately formatted tags that unveil.js can handle
    * This also runs a check for the wp-retina-2x plugin. If found, the appropriate retina data attributes will also be added.
    *
    * @since 1.0.0
    *
    * @param string $content    Content
    * @return string            Modified Content
    */
    function replace_images( $content ) {

        // Smallest, most stable, 1 by 1 pixel transparent image
        $placeholder = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        
        // Replace all image tags with the appropriately formatted HTML
        // We add a data-unveil attribute so we can target a CSS transition (fade-in)
        $content = preg_replace( '#<img([^>]+?)src=[\'"]?([^\'"\s>]+)[\'"]?([^>]*)>#', sprintf( '<img${1}src="%s" data-src="${2}"${3} data-unveil="true"><noscript><img${1}src="${2}"${3}></noscript>', $placeholder ), $content );

        // Return
        return $content;
        
    }

}

// Initialise the class
$image_lazy_load = new Image_Lazy_Load();