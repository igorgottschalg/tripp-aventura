<?php
$_SERVER['PHP_AUTH_USER'] = 'trippaventura';
$_SERVER['PHP_AUTH_PW'] = '1234';
function doAuthenticate() {
  if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
    if ($_SERVER['PHP_AUTH_USER'] == "trippaventura" && $_SERVER['PHP_AUTH_PW'] == "1234")
    return true;
    else
    return false;
  }
}

if(!doAuthenticate()){
  echo json_encode(array(
    "msg"       => "authentication fail",
    "code"      => 0
  ));
  exit();
}
global $wpdb;
require( '../wp-load.php' );

$sku = $_POST['sku'];
$new_stock_level = intval($_POST['new_stock_level']);
$price = $_POST['price'];
$qt_sale = intval($_POST['qt_sale']);

if($sku == null){
  echo json_encode(array(
    "msg"       => "Sku is empty",
    "code"      => 3
  ));
  exit();
}

  $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
  $product = wc_get_product( $product_id );
//  echo $product;

  if ($product){
      if ( ! metadata_exists( 'post', $product_id, '_stock' ) || $product->get_stock_quantity() !== $new_stock_level ) {
        $product->set_stock( $new_stock_level );
        
        $returnData = array(
                "sku" => $sku,
                "new_stock_level" => $new_stock_level,
                "product_exists" => true,
                "product_updated" => true,
                "msg" => "Product QUANTITY updated",
                "createdAt" => date('Y-m-d H:i:s')
            );
          echo "[". json_encode( $returnData ) ."]";
      }

      if ($price){
	  update_post_meta( $product_id, '_regular_price', $price );
          update_post_meta( $product_id, '_price', $price );
	  wc_delete_product_transients( $product_id );
          $returnData = array(
                "sku" => $sku,
                "new_stock_level" => $new_stock_level,
                "product_exists" => true,
                "product_updated" => true,
                "msg" => "Product PRICE updated",
		"price" => $price,
                "createdAt" => date('Y-m-d H:i:s')
            );
          echo "[". json_encode( $returnData ) ."]";
      }
    exit();  
  }
  else { //Produto não existe e não é atualizado
          $returnData = array(
              "sku" => $sku,
              "new_stock_level" => $new_stock_level,
              "product_exists" => false,
              "product_updated" => false,
              "createdAt" => date('Y-m-d H:i:s')
          );
          echo "[". json_encode( $returnData ) ."]";
  }
 ?>
