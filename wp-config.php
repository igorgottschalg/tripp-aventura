<?php
/**
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */


define('FS_METHOD','direct');
// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'tripp_beta');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'trippaventura');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'trippaventura');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X@x~pQIW-ur{/G.s1R+U.u?1B)O@*sRn5{s{@fpA!V{8k~QH/>kOQ7(nTDS9V-:z');
define('SECURE_AUTH_KEY',  'W0)x(i0Bvn?{k7VvBZ8ah3rW+^Za- eBG_{]3/ PsDbK! [X/-p^P)UJB@&+[m}J');
define('LOGGED_IN_KEY',    'H*L6:N(_>WcQt-PcbVzDkX/:{n5Om:3:t:|J])SC-@uN_]aD7G.UuoJbUkTcBS4X');
define('NONCE_KEY',        '^:kI.i!o#5IvM)oc,#%UCI0e_6|.eBsx]~bhu7dQt,&z]%q/-)#N-buJr6lAxLm}');
define('AUTH_SALT',        'jcYXrxN_8NspLPzD@*DzdT~*s+%ntZ;lZR-CaKqU[+;XW|XY[e;<9v3M]:-DqLt)');
define('SECURE_AUTH_SALT', '0K}BB)=Y77P+(dGp>}5+?<Txv;$4X+D]&G@JtQ~>0E+3tNFX(OG4v4S0,zz@Q{n`');
define('LOGGED_IN_SALT',   '4cj*8@+U&c+qMA1e`ySX4 YH8bas9dur99< jA/[JRxzSF Bj;y-?Z={Gx6(M7Y!');
define('NONCE_SALT',       'Ybd](*W!We4n{sJIHv8*C=UJK7XY^sA1)lsPQ)~L(Gv$+|6+rj-V-6yY46b2i{l>');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
